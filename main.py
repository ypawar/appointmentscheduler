import time
from datetime import datetime
from flask import Flask, render_template, request, session
import json
import sqlite3 as db
from flask_mail import Mail, Message

con = db.connect('appointmentScheduler.sqlite', check_same_thread=False)
cur = con.cursor()

#Flask applicaion object
app = Flask(__name__)

#To encrypt the session variables
app.secret_key = 'randomstring'

department = []
agendas = []
# App Specific Password for sending emails to the users
myid = "youremail@domain.com"
password = "app_specific_password"
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = myid
app.config['MAIL_PASSWORD'] = password
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_DEFAULT_SENDER'] = myid

mail = Mail(app)

# Email strings
requestedEmail = '''Stevens Appointment Scheduler
Hello {0},
Your appointment request has been registered, wait for the respective person to approve it.
Details of the appointment:
Faculty :   {1}
Location:   {2}
Date    :   {3}
Time    :   {4}
Agenda  :   {5}
Comments:   {6}'''

approvedEmail = '''Stevens Appointment Scheduler
Hello {0},
Your appointment request has been approved.
Details of the appointment:
Faculty :   {1}
Location:   {2}
Date    :   {3}
Time    :   {4}
Agenda  :   {5}
Comments:   {6}'''

rejectedEmail = '''Stevens Appointment Scheduler
Hello {0},
Your appointment request has been rejected by {1}.
Details of the appointment:
Faculty :   {1}
Location:   {2}
Date    :   {3}
Time    :   {4}
Agenda  :   {5}
Comments:   {6}'''

cancelledEmail = '''Stevens Appointment Scheduler
Hello {0},
Your scheduled appointment request has been cancelled by {1}.
Details of the appointment:
Faculty :   {1}
Location:   {2}
Date    :   {3}
Time    :   {4}
Agenda  :   {5}
Comments:   {6}'''


@app.route("/", methods=['POST', 'GET'])
def home():
    # return "Home Page"
    if request.method == 'GET':
        session.clear()
        return render_template('index.html', js="homepage.js", d=department)
    else:
        faculty = request.form['faculty']
        fid = int(request.form['fid'])
        print faculty
        print fid
        session['faculty'] = faculty
        session['fid'] = fid
        return ("OK")


@app.route("/dateselection", methods=['POST', 'GET'])
def dateselection():
    # return "Date Selection Page"
    try:
        print session['fid']
        if request.method == 'GET':
            return render_template('dateSelection.html', js="dateSelection.js")
        else:
            scheduled = int(request.form['scheduled'])
            # print type(scheduled)
            if checkAvailable(session['fid'], scheduled):
                session['scheduled'] = scheduled
                return ("OK")
            else:
                return ("NA")
    except KeyError as e:
        return "Not Allowed!"


@app.route("/appointmentdetails", methods=['POST', 'GET'])
def appointmentdetails():
    # return "Date Selection Page"
    try:
        print session
        print session['faculty']
        print session['fid']
        print session['scheduled']
        if request.method == 'GET':
            return render_template('adetails.html', js="details.js", ag=agendas)
        else:
            if request.form['name']:
                name = request.form['name']
                email = request.form['email']
                agenda = int(request.form['agenda'])
                comment = request.form['comment']
                if checkAvailable(session['fid'], session['scheduled']):
                    # add appointment details to database and return ok
                    # Add applicant details
                    now = int(time.time())
                    with con:
                        print "adding applicant"
                        cur.execute('''insert into applicant (name,email,created) values(?,?,?)''', (name, email, now))
                        # get applicant id
                        cur.execute('''select max(id) from applicant''')
                        aid = cur.fetchone()[0]
                        print aid
                        # add rest details in appointment table
                        print "Adding apointment"
                        cur.execute(
                            'insert into appointment (faculty,applicant,agenda,comments,status,scheduled,requested) values(?,?,?,?,?,?,?)',
                            (session['fid'], aid, agenda, comment, "pending", session['scheduled'], now))
                    # Send email to the applicant that request has been registered
                    date = datetime.fromtimestamp(session['scheduled']).strftime("%d %B %Y")
                    Time = datetime.fromtimestamp(session['scheduled']).strftime("%I:%M %p")
                    with con:
                        cur.execute('select name from agenda where id=?', (agenda,))
                        Agenda = cur.fetchone()[0]
                    sendEmail(email, "Stevens Appointment Scheduler",
                              requestedEmail.format(name, session['faculty'], getLocation(session['fid']), date, Time,
                                                    Agenda, comment))
                    return ("OK")
                else:
                    return ("NA")
            else:
                # print request.form['q']
                response = {
                    'faculty': session['faculty'],
                    'date': session['scheduled']
                }
                print response
                return (json.dumps(response))
    except KeyError as e:
        return "Not Allowed!"


@app.route("/dashboard", methods=['GET', 'POST'])
def dashboard():
    if request.method == 'GET':
        try:
            return render_template("dashboard.html", js="dashboard.js", user=session['loginid'], dept=department)
        except KeyError as e:
            return ("Please login properly!")


@app.route("/authenticate", methods=['GET', 'POST'])
def authenticate():
    if request.method == 'POST':
        id = request.form['id']
        passwd = request.form['password']
        print "Received" + id + " " + passwd
        if checkUser(id, passwd):
            session['loginid'] = id
            print "Sending OK"
            checkExpired()
            return ("OK")
        else:
            print "Sending NA"
            return ("NA")


@app.route("/logout", methods=['POST'])
def logout():
    if request.method == 'POST':
        id = request.form['loginid']
        print "ID:" + str(id)
        if (session['loginid'] == id):
            session.clear()
            return "OK"


@app.route("/getdepartment")
def getDepartment():
    return (response)


@app.route("/getdetails")
def getdetails():
    try:
        location = getLocation(session['fid'])
        response = {
            'faculty': session['faculty'],
            'date': session['scheduled'],
            'location': location
        }
        print response
        return (json.dumps(response))
    except KeyError as e:
        return "Not Allowed!"


@app.route("/getdata", methods=['POST', 'GET'])
def getdata():  # get data by loginid of the user
    if request.method == 'GET':
        query = "select id from faculty where loginid=?"
        user = session['loginid']
        if (user != 'admin'):
            with con:
                cur.execute(query, (user,))
                fid = cur.fetchone()[0]
                print "fid " + str(fid)
        if (user != 'admin'):
            query = "select ao.id,f.name fname,al.name aname,al.email aemail,ag.name agenda,ao.comments,ao.status,ao.scheduled from appointment ao join faculty f on ao.faculty = f.id join applicant al on ao.applicant=al.id join agenda ag on ao.agenda=ag.id where ao.faculty=?"
            with con:
                cur.execute(query, (fid,))
                table = cur.fetchall()
        else:
            query = "select ao.id,f.name fname,al.name aname,al.email aemail,ag.name agenda,ao.comments,ao.status,ao.scheduled from appointment ao join faculty f on ao.faculty = f.id join applicant al on ao.applicant=al.id join agenda ag on ao.agenda=ag.id"
            with con:
                cur.execute(query)
                table = cur.fetchall()
        print "Table " + str(table)
        data = {'data': table}
        return (json.dumps(data))
    else:
        action = request.form["action"]
        aid = int(request.form["aid"])

        query = "select al.name,al.email,f.name,f.location,ao.scheduled,ag.name,ao.comments from appointment ao join faculty f on ao.faculty = f.id join applicant al on ao.applicant=al.id join agenda ag on ao.agenda=ag.id where ao.id=?"
        with con:
            cur.execute(query, (aid,))
            result = cur.fetchone()
        print action
        print aid
        print "Result" + str(result)
        date = datetime.fromtimestamp(result[4]).strftime("%d %B %Y")
        Time = datetime.fromtimestamp(result[4]).strftime("%I:%M %p")
        if (action == "approve"):
            query = '''update appointment set status='approved' ,approved=? where id=?'''
            message = approvedEmail
        elif (action == "reject"):
            query = '''update appointment set status='rejected' ,rejected=? where id=?'''
            message = rejectedEmail
        elif (action == "cancel"):
            query = '''update appointment set status='cancelled' ,cancelled=? where id=?'''
            message = cancelledEmail
        else:
            return ("BA")  # Bad action
        with con:
            cur.execute(query, (int(time.time()), aid))
        sendEmail(result[1], "Stevens Appointment Scheduler", message.format(result[0], result[2], result[3],
                                                                             date, Time, result[5], result[6]))
        return "OK"


@app.route("/getuser", methods=['GET'])
def getuser():
    try:
        result = {"user": session['loginid']}
        print "User" + str(result)
        return json.dumps(result)
    except KeyError as e:
        return "BR"


def init():
    # Get departments and pack into to dictionary
    with con:
        cur.execute('select id,name from department where active=1')
        d = cur.fetchall()
    global department
    for row in d:
        with con:
            cur.execute('select id,name from faculty where active=1 and department=?', (row[0],))
            faculty = cur.fetchall()
        department.append(
            {
                'id': row[0],
                'name': row[1],
                'faculty': faculty
            }
        )
    global response
    response = json.dumps(department)
    # Get agendas and pack into to dictionary
    with con:
        cur.execute('''select distinct(type) from agenda''')
        types = cur.fetchall()
    # print types
    # create a list of agendas based on type
    for t in types:
        list = []
        with con:
            cur.execute('''select id,name from agenda where type=?''', (t[0],))
            l = cur.fetchall()
        global agendas
        agendas.append(
            {
                'type': t[0],
                'list': l
            }
        )
    print "agendas" + str(agendas)
    checkExpired()


def getLocation(fid):
    with con:
        cur.execute("select location from faculty where id=?", (fid,))
        location = cur.fetchone()[0]
    return location


def checkAvailable(facultyid, Time):
    with con:
        cur.execute('''select * from appointment where status in(?,?) and faculty=? and scheduled=?''',
                    ("pending", "approved", facultyid, Time))
        l = cur.fetchall()
        # print l
        if (len(l) == 0):
            return True
    return False


def sendEmail(to, subject, message):
    try:
        msg = Message(subject=subject, recipients=[to])
        msg.body = message
        mail.send(msg)
    except e:
        print e


def checkUser(id, pwd):
    if (id == 'admin' and pwd == 'admin@123'): return True
    with con:
        cur.execute('''select id from faculty where loginid=? and password=?''', (id, pwd))
        if (len(cur.fetchall()) > 0):
            return True
    return False


# Expire appointmnets whose scheduled date is less than current date
def checkExpired():
    query = "select id from appointment where scheduled < ?"
    with con:
        cur.execute(query, (int(time.time()),))
        r = cur.fetchall()
    query = "update appointment set status='expired' where id=?"
    if (len(r) > 0):
        for id in r:
            with con:
                cur.execute(query, (id[0],))


@app.route("/adduser", methods=['POST'])
def addUser():
    if (session['loginid'] == 'admin'):
        name = request.form['name']
        designation = request.form['designation']
        location = request.form['location']
        phone = request.form['phone']
        fax = request.form['fax']
        email = request.form['email']
        loginid = request.form['loginid']
        password = request.form['password']
        dept = int(request.form['department'])
        aquery = "insert into faculty (name,designation,location,phone,fax,email,loginid,password,department,created,active) values(?,?,?,?,?,?,?,?,?,?,?)"
        with con:
            cur.execute(aquery,
                        (name, designation, location, phone, fax, email, loginid, password, dept, int(time.time()), 1))
        return "OK"
    else:
        return "Please login properly!"


@app.route("/adddepartment", methods=['POST'])
def addDept():
    if (session['loginid'] == 'admin'):
        dname = request.form['name']
        # Check if already exists else add
        cquery = '''select id from department where name=?'''
        aquery = '''insert into department (name,created,active) values(?,?,?)'''
        with con:
            cur.execute(cquery, (dname,))
            if (len(cur.fetchall()) > 0):
                return "EXISTS"
            else:
                cur.execute(aquery, (dname, int(time.time()), 1))
                return "OK"
    else:
        return "Please login properly!"


@app.route("/addagenda", methods=['POST'])
def addAgenda():
    if (session['loginid'] == 'admin'):
        agname = request.form['name']
        agtype = request.form['type']
        with con:
            cur.execute('insert into agenda (type,name) values(?,?)', (agtype, agname))
            return "OK"
    else:
        return "Please login properly!"


# Department and agenda is read from db only at starting of the application, any changes requires application restart
init()
app.run(debug=True, host='localhost', port=80)
