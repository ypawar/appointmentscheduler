$(document).ready(function () {
	$("#incorrect").hide();
	var dept = "";
  $.getJSON('getdepartment',function(data){
	  dept=data;
	  displayDepartments(dept);
	  }
	);
	
	$("#next").click(function(){
		f = $( "#selectfaculty option:selected" ).text();
		fid = $( "#selectfaculty option:selected" ).val();
		console.log(f)
		$.post("/",{'faculty':f,'fid':fid},function(data,status){
			if (data=="OK") {
			console.log(status)
			window.location.href="/dateselection"
			}
			})		
		
		})
		
	$("#lgbtn").click(function(){
		$("#loginModal").modal()
	});
	
	
	var validator = $("#loginform").validate(
	{
		rules:{
			loginind:{required:true,minlength:2,maxlength:20},
			password:{required:true,minlength:6,maxlength:20}			
		}
	}
	);
			
	$("#login").click(function(){
		console.log("Clicked login button")
		if(validator.form())
		{
			authenticate();
		}
		});
	
});
function displayDepartments(d)
{	
	/* div = '';
	for (i = 0;i<d.length;i++)
	{	
		if (i==0)
		{
			div+='<div class="radio"><label for="dept-' + i +'"><input name="dept" id = "dept-' + i + '" ' + 'value="' + i + '" type="radio"' + ' checked="checked"'+ '>' + d[i].name + '</label></div>'
		}
		else 
		div+='<div class="radio"><label for="dept-' + i +'"><input name="dept" id = "dept-' + i + '" ' + 'value="' + i + '" type="radio">' + d[i].name + '</label></div>'
	}
	$("#departments").append(div); */
	displayFaculty(d,0);
	$("input[name='dept']").click(function () {displayFaculty(d,$(this).val())});
}
function displayFaculty(dept,id)
{
	console.log(id)
	options='';
	for (i=0;i<dept[id].faculty.length;i++)
	{
		options+='<option value="' + dept[id].faculty[i][0] + '">' + dept[id].faculty[i][1] +'</option>'
	}
	$("#selectfaculty").empty();
	$("#selectfaculty").append(options);
}

function authenticate()
{
	$("#incorrect").hide();
	id = $("#loginind").val().trim();
	password = $("#password").val().trim();
	d = {'id':id,'password':password};
	$.post("/authenticate",d,function(data,status){
		console.log("Sent Data")
		if (data=="OK")
		{
			console.log("Login Successfull! for user : " + id);
			//Redirect to the dashbaord
			window.location.href="/dashboard";
		}
		else 
		{
			console.log("Error in login")
			$("#incorrect").show();
		}
	});
}