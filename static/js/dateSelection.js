$(document).ready(function () {
	today = moment().format("YYYY-MM-DD");
	mindate= moment(moment(today).add(1,"d")).hour(10).minute(0)
	$("#sdate").val(mindate.format("dddd, MMMM D, YYYY, h:mm A"));
	$('#example').datetimepicker({inline: true,sideBySide: true,keepOpen:true,minDate:mindate,defaultDate:mindate});
	$('#example').data("DateTimePicker").format("dddd, MMMM D, YYYY, h:mm A")
	$('#example').data("DateTimePicker").stepping(30)
	$('#example').data("DateTimePicker").enabledHours([10, 11, 12, 13, 14, 15, 16])		
	//$('#example').data("DateTimePicker").minDate(today);
	$('#example').data("DateTimePicker").maxDate(moment(today).add(1,'M'));
	$('body').bind('dp.change', function(e)
	{
		d = $('#example').data("DateTimePicker").date();
		//console.log($('#example').data("DateTimePicker").date().unix()+19800)
		$("#sdate").val(d.format("dddd, MMMM D, YYYY, h:mm A"));
		//console.log(d.toString());
	});
	
	$("#next").click(function(){
	s = $("#sdate").val();
	//console.log(s)
	scheduled = new Date(s).getTime()/1000
	console.log(scheduled);
	$.post("/dateselection",{'scheduled':scheduled},function(data,status){
			if (data=="OK") {
			console.log(status)
			window.location.href="/appointmentdetails"
			}
			if (data=="NA"){
				displayNA();
			}
		})	
	
	//$.getJSON('checkslot')
	});
});

function displayNA()
{
	$("#myModal").modal()
}