$(document).ready(function(){
$("#deptexists").hide()

//get user name
var user;
$.get("/getuser",function(data,status){
	
	if (status=="success")
	{		
		if (data=="BR"){document.write("Please log in properly");die("Error")}
		else
		{
			console.log(data)
			data=JSON.parse(data)
			user=data.user;
			console.log("user " + user)
			if (user!='admin') 
			{
				visibility = {"visible":false,"targets":[0,1]}
			}
			else visibility = {"visible":false,"targets":0}
			str = "Logged in as " + user
			console.log(str)
			$("#loggeduser").html(str)
			var table = $("#appointmentsdata").DataTable({
															"ajax":"/getdata",
															"columnDefs":[visibility,
															{ "width": "130px", "targets": 8 },
															{targets:8,
															searchable:false,
															orderbale:false,
															data:null,
															render: function(data,type,row,meta){
																if (type==='display'){
																	if (row[6]=='pending'){
																		data = '<div class="text-center"><div class="btn-group"><button id="approve" class="btn btn-success">Approve</button>\
																		<button id="reject" class="btn btn-danger">Reject</button></div></div>'
																	}
																	else if(row[6]=='approved'){
																		data = '<button id="cancel" class="btn btn-warning center-block">Cancel</button>'
																	}
																	else data=null;
																}
																return data;
															}},
															{targets:7,
															/*
															render:function(data,type,row,meta){
																/* if (type==='display'){
																data=moment.unix(row[7]).format("D MMM YYYY h:mm A")} 
																return row[7]
																}}*/
															render: $.fn.dataTable.render.moment( 'X', 'D MMM YYYY h:mm A' )
															}],})
		
	//Update status in DB as well as table 
	$('#appointmentsdata tbody').on( 'click', 'button', function () {
		var trow = table.row( $(this).parents('tr') )
		var rdata = trow.data();
		var btnid = this.id; // get the id of the button clicked
		var aid = rdata[0]
		if (btnid=="approve"){
			$.post("/getdata",{"action":"approve","aid":aid},function(data,status){
				if(data=="OK"){
					console.log("approved appointment with id " + aid)
					rdata[6]="approved"
					trow.data(rdata).draw();
				}				
				else console.log("Failed to approve the appointmnet by id: " + aid)
			})};
			
		if (btnid=="reject"){
			$.post("/getdata",{"action":"reject","aid":aid},function(data,status){
				if(data=="OK"){
					console.log("rejected appointment with id " + aid)
					rdata[6]="rejected"
					trow.data(rdata).draw();
				}
				else console.log("Failed to reject the appointmnet by id: " + aid)
			})};
			
		if (btnid=="cancel"){
			$.post("/getdata",{"action":"cancel","aid":aid},function(data,status){
				if(data=="OK"){
					console.log("cancelled appointment with id " + aid)
					rdata[6]="cancelled"
					trow.data(rdata).draw();
				}
				else console.log("Failed to cancel the appointmnet by id: " + aid)
			})};
		});
		}
	
	}	
});
	
	$("#lgobtn").click(function(){
		$.post("/logout",{"loginid":user},function(data,status){
			if (data=="OK")
				console.log("Logged out " + user)
			window.location.href="/"
		});
	});
	
	$("#adduserbtn").click(function(){$("#addUserModal").modal();})
	$("#adddeptbtn").click(function(){$("#addDeptModal").modal();})
	$("#addagbtn").click(function(){$("#addAgModal").modal();})
	
	
	
	var userform = $("#userform").validate(
	{
		rules:{
			name:{required:true,minlength:2,maxlength:20},
			designation:{required:true,minlength:2,maxlength:20},
			location:{required:true,minlength:2,maxlength:20},
			phone:{required:true,minlength:2,maxlength:20},
			fax:{required:false,minlength:2,maxlength:20},
			loginid:{required:true,minlength:6,maxlength:20},
			password:{required:true,minlength:6,maxlength:20},
			email:{required:true,minlength:6,maxlength:40,email:true},
			selectdept:{required:true},
		}
	}
	);
	
	var deptform = $("#deptform").validate(
	{
		rules:{
			deptname:{required:true,minlength:2,maxlength:20}	
		}
	}
	);
	
	var agform = $("#agform").validate(
	{
		rules:{
			agtype:{required:true,minlength:2,maxlength:20},
			agname:{required:true,minlength:2,maxlength:20}
		}
	}
	);
	
	$("#adduser").click(function(){
		console.log("Clicked add user")
		if(userform.form()){
			name=$("#uname").val()
			designation=$("#designation").val()
			lctn=$("#location").val()
			phone=$("#phone").val()
			fax=$("#fax").val()
			email=$("#email").val()
			loginid=$("#loginid").val()
			password=$("#password").val()
			department = $( "#selectdept option:selected" ).val();
			sendata={'name':name,'designation':designation,'location':lctn,'phone':phone,'fax':fax,'email':email,'loginid':loginid,'password':password,'department':department};
			$.post("/adduser",sendata,function(data,status){
				if(data=="OK")
				{
					$('#addUserModal').modal('hide');					
				}
			})
			}
		})
		
	$("#adddept").click(function(){
		console.log("Clicked add dept")
		if(deptform.form()){
			$.post("/adddepartment",{'name':$("#deptname").val()},function(data,status){
				if(data=="OK")
				{
					$('#addDeptModal').modal('hide');
					$("#deptexists").hide()
				}
				if(data=="EXISTS")
				{
					$("#deptexists").show()
				}
			})
			}
		})
		
	$("#addag").click(function(){
		console.log("Clicked add agenda")
		if(agform.form()){
			$.post("/addagenda",{'name':$("#agname").val(),'type':$("#agtype").val()},function(data,status){
			if(data=="OK")
			{
				$('#addAgModal').modal('hide');				
			}
			/*
			if(data=="EXISTS")
			{
				
			}*/
			})
			}
		})	
});