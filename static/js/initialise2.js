$(document).ready(function () {
    $("#adduri").click(adduri)
	globalstat();
	getall();	
	setInterval(globalstat, 5000);
	setInterval(getall, 5000);
})

function adduri()
{
	window.location.replace("adduri");
}

function getall()
{
	$.getJSON('getall', function(data,status)
	{
		console.log(data[0].id)
		drawtable((data))
	}
	);  
}

function drawtable(data)
{
	//Empty the table first
	$("tbody").empty();
	//Add rows to the table
	var tablerow=""
	/* for (var i=0;i<data.length;i++)
	{	tablerow+="<tr>";
		tablerow+="<td>" + data[i].id + "</td>";
		tablerow+="<td>" + data[i].gid + "</td>";		
		tablerow+='<td style="overf-wrap: break-word;max-width: 160px;">' + data[i].uri + "</td>";		
		tablerow+="<td>" + data[i].status + "</td>";		
		tablerow+="<td>" + data[i].path + "</td>";
		if (data[i].path.length > 1) 
		{
			tablerow+="<td>" + data[i].path.split("/").pop() + "</td>";		
		}
		else tablerow+="<td>" + "Unknown" + "</td>";		
		tablerow+="</tr>";
	} */
	for (var i=0;i<data.length;i++)
	{	tablerow+="<tr>";
		//FileName from path
		if (data[i].path.length > 1) 
		{
			tablerow+="<td>" + data[i].path.split("/").pop() + "</td>";		
		}
		else tablerow+="<td>" + "Unknown" + "</td>";
		tablerow+="<td>" + data[i].status + "</td>";
		tablerow+="<td>" + filesize(parseInt(data[i].totalLength)) + "</td>";
		//Progressbar
		if (data[i].status=="complete")
		{
			tablerow+="<td><br><div class='progress'><div class='progress-bar progress-bar-success' role='progressbar' style='width:50%'>" +"<span style='color:black'>50%<span>" +"</div></div></td>";
		}
		/*
		else 
		{
			
		}
		*/
		//Speed
		tablerow+="<td></td>";
		//ETA
		tablerow+="<td></td>";
		//Connection
		tablerow+="<td></td>";
		tablerow+="</tr>";
	}
	
	console.log(tablerow);
	$("tbody").append(tablerow);
	
}

function globalstat()
{
	$.get('globalstat',function(data,status)
	{
		data = JSON.parse(data)
		$('#dspeed').html(data["d"])
		$('#uspeed').html(data["u"])
		/* console.log(data["d"])
		console.log(status) */
	});
}