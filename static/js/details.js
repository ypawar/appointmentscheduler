$(document).ready(function()
{
	$.getJSON("/getdetails",function(data,status){
		$("#faculty").html(data.faculty);
		$("#location").html(data.location);
		utime = parseInt(data.date);
		$("#date").html(moment.unix(utime).format("dddd, MMMM Do YYYY"));
		$("#time").html(moment.unix(utime).format("h:mm A"));	
	});
	//Add not equal to rule to the plugin
	jQuery.validator.addMethod("notEqual", function(value, element, param) {
 return this.optional(element) || value != $(param).val();
}, "Last Name has to be different than First Name");
	var validation = $("#dform").validate(
	{
		rules:{
			fname:{required:true,minlength:2,maxlength:20},
			lname:{required:true,minlength:2,maxlength:20,notEqual:'#fname'},
			email:{required:true,minlength:6,maxlength:30,email:true},
			agenda:{required:true},
		}
	}
	);
	$("#submit").click(function(){
		if(validation.form()){sendData()}});
}
);

function sendData()
{
	var name= $("#fname").val().trim() + " " + $("#lname").val().trim()
	var email = $("#email").val().trim()
	var agenda = $('input:radio[name=agenda]:checked').val();
	var comment = $("#comments").val().trim()
	console.log(name)
	console.log(email)
	console.log(agenda)
	console.log(comment)
	$.post("/appointmentdetails",{'name':name,'email':email,'agenda':agenda,'comment':comment},function(data,status){
		if(data=="OK")
		{
			$("#myModal").modal()
			$("#myModal").on('hidden.bs.modal', function () {
            window.location.href="/";
			});
		}
		if (data=="NA")
		{
			$("#myModal2").modal()
		}
	});
	
	//console.log($('input:radio[name=agenda]:checked').val());
	//alert($('input:radio[name=agenda]:checked').val());
}